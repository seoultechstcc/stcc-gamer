const path = require('path');
const webpack = require('webpack');

const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

let envDefinition = {

};

module.exports = {
    mode: 'development',
    // mode: 'production',
    devtool: 'source-map',
    entry: {
        app: './src/entry.js',
    },
    module: {
        rules: [{
            test: [/\.ico$/],
            loader: 'file-loader',
            options: {
                name: '[name].[ext]'
            }
        }, {
            test: [/images/],
            loader: 'file-loader',
            options: {
                name: 'images/[name].[ext]'
            }
        }, {
            test: /\.ts$/,
            use: 'awesome-typescript-loader'
        }, {
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            query: {
                presets: [
                    'env'
                ]
            }
        }, {
            test: /\.html$/,
            exclude: [/\/src\/index\.template\.html/],
            loader: "ng-cache-loader?prefix=[dir]/[dir]"
        }, {
            test: [/\.css$/, /\.scss$/],
            use: [
                {
                    loader: MiniCssExtractPlugin.loader
                }, {
                    loader: 'css-loader'
                }, {
                    loader: 'sass-loader',
                    options: {
                        sourceMap: true,
                        data: '@import "./stylesheets/theme.scss";',
                        includePaths: [
                            path.join(__dirname, 'src')
                        ]
                    }
                }
            ]
        }, {
            enforce: "pre",
            test: /\.js$/,
            loader: 'source-map-loader'
        }]
    },
    resolve: {
        extensions: ['.ts', '.js', '.json'],
    },
    plugins: [
        new CleanWebpackPlugin(['public/dist']),
        new webpack.DefinePlugin(envDefinition),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'src/index.template.html',
            inject: true
        }),
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[name].css'
        }),
        new webpack.ProvidePlugin({
            '$': 'jquery',
            'window.jQuery': 'jquery',
            'moment': 'moment',
        })
    ],
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'public/dist'),
        publicPath: "/dist/"
    },
    optimization: {
        splitChunks: {
            chunks: 'all'
        }
    }
};