'use strict';

const passport = require('passport');
const jwt = require('jsonwebtoken');
const passportJwt = require('passport-jwt');

const config = require('../../../server.config');

const ExtractJwt = passportJwt.ExtractJwt;
const JwtStrategy = passportJwt.Strategy;

const ACCESS_TOKEN_EXPIRY = 60; // minute
const REFRESH_TOKEN_EXPIRY = 14; // day

/* JWT Auth Logic */
passport.use(new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: config.jwt_token_secret,
}, (jwt_payload, done) => {
    if(jwt_payload.data){
        let token_info = jwt_payload.data;
        token_info.expires_in = jwt_payload.exp;
        return done(null, token_info);
    } else {
        let err = new Error('Bad Request');
        err.code = 'ERROR_TOKEN';
        err.status = 400;
        return done(err, null);
    }
}));

/**
 * Access Token 유효 기간 세트
 * @returns {number} 
 */
function makeAccessTokenExpire(){
    return Math.floor(Date.now() / 1000) + (ACCESS_TOKEN_EXPIRY * 60);
    // return Math.floor(Date.now() / 1000) + (1);  // Test 1 Sec
};

/**
 * Refresh Token 유효 기간 세트
 * @returns {number} 
 */
function makeRefreshTokenExpire(){
    return Math.floor(Date.now() / 1000) + (REFRESH_TOKEN_EXPIRY * 24 * 60 * 60);
    // return Math.floor(Date.now() / 1000) + (5);  // Test 5 Sec
};

/**
 * @typedef AccessTokenPayload
 * @param {number} seq 시퀀스명
 */

/**
 * Access Token 발급
 * @param {number} res.locals.seq
 * @property {string} access_token
 * @property {number} access_expires
 */
function makeAccessToken (req, res, next) {
    /** @type {AccessTokenPayload} */
    let payload = {
        seq: res.locals.seq,
    };

    res.locals.access_expires = makeAccessTokenExpire();
    res.locals.access_token = jwt.sign({
        exp: res.locals.access_expires,
        data: payload
    }, config.jwt_token_secret);
    return next(null);
};

/** 
 * Refresh Token 발급
 * @param {number} res.locals.seq
 * @property {string} refresh_token
 * @property {number} refresh_expires
 */
function makeRefreshToken (req, res, next) {
    /** @type {AccessTokenPayload} */
    let payload = {
        seq: res.locals.seq,
    };
    res.locals.refresh_expires = makeRefreshTokenExpire();
    res.locals.refresh_token = jwt.sign({
        exp: res.locals.refresh_expires,
        data: payload
    }, config.jwt_token_secret);
    return next(null);
};

/**
 * jwt 토큰 해독
 * @param {string} res.locals.token 해독할 토큰 
 * @property {object} token_data 해독 후 토큰에 저장된 데이터
 */
function decodeJwt (req, res, next) {
    jwt.verify(res.locals.token, config.jwt_token_secret, (err, jwt_payload) => {
        if(err) {
            let error = new Error('Forbidden');
            error.status = 403;
            return next(error);
        } else {
            res.locals.token_data = jwt_payload.data;
            return next(null);
        }
    });
}

/**
 * Bearer 토큰 인증 로직
 * 인증되지 않으면 req.user 에는 null 이 들어가며 에러를 반환하지는 않음.
 */
function decodeToken (req, res, next) {
    passport.authenticate('jwt', {
        session: false
    }, (err, user, info) => {
        if(err){
            let e = new Error('Internal Server Error');
            e.status = 500;
            return next(e);
        } else if (!user){
            req.user = null;
            return next(null);
        } else {
            req.user = user;
            return next(null);
        }
    })(req, res, next);
}

/**
 * Bearer 토큰 인증 로직
 * 인증되지 않으면 401 에러를 반환한다.
 */
function authorizeToken (req, res, next) {
    passport.authenticate('jwt', {
        session: false
    }, (err, user, info) => {
        if(err){
            let e = new Error('Internal Server Error');
            e.status = 500;
            return next(e);
        } else if (!user){
            let e = new Error('Unauthorized');
            e.status = 401;
            e.code = 'ERROR_TOKEN_EXPIRED';
            e.reason = info.message;
            return next(e);
        } else {
            req.user = user;
            return next(null);
        }
    })(req, res, next);
}

module.exports = {
    decodeJwt,
    makeAccessToken,
    makeRefreshToken,
    decodeToken,
    authorizeToken,
}