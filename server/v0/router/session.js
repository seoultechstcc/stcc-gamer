'use strict';

const express = require('express');
const async = require('async');

const db = require('@dobedo-online-studio/prelude-mysql');

const authUtil = require('../util/passport');

let router = express.Router();

/**
 * seq 를 통해 사용자 세션 정보를 획득
 */
function getSession (req, res, next) {
    let selectUserSessionSQLString = 'SELECT seq, discord_id, displayname, last_login_datetime, email, group_seq, status_code FROM users_tbl WHERE status_code!="X" AND seq=?;';
    async.waterfall([
        (nextCallback) => {
            req.sqlConnection.query({
                sql: selectUserSessionSQLString,
                values: [
                    req.user.seq
                ]
            }, nextCallback);
        }, (result, fields, nextCallback) => {
            if (result.length === 0) {
                let e = new Error('Bad Request');
                e.status = 400;
                e.code = 'ERROR_INVALID_USER';
                return nextCallback(e);
            } else {
                res.locals.user = result[0];
                return nextCallback(null);
            }
        }
    ], err => next(err));
}

router.get('/',
    authUtil.authorizeToken,
    db.getConnection,
    getSession,
    db.endConnection,
    (req, res) => {
        return res.send(res.locals.user);
    }
);

module.exports = {
    router
};