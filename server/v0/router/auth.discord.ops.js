'use strict';

const async = require('async');
const request = require('request');

const config = require('../../../server.config');

/**
 * 주의사항
 * 1. 로그인 창을 통해 발급받는 code 는 일회용으로, 한 번 인증 처리를 거치면 폐기된다.
 * 2. x-www-form-urlencoded 의 경우 request 모듈에서 form 을 사용하면 됨.
 * 3. 받아오는 데이터는 JSON String 임을 주의
 */

/**
 * 인증키를 받아서 Discord API 서버로 Access Token 발급 받음
 */
function getDiscordInfo (req, res, next) {
    async.waterfall([
        (nextCallback) => {
            request('https://discordapp.com/api/v6/oauth2/token', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json'
                },
                form: {
                    client_id: config.discord.client,
                    client_secret: config.discord.secret,
                    grant_type: 'authorization_code',
                    code: req.body.code,
                    redirect_uri: req.body.redirect_uri
                }
            }, nextCallback);
        }, (result, body, nextCallback) => {
            if(result.statusCode !== 200) {
                let e = new Error('Forbidden');
                e.status = 403;
                e.code = 'ERROR_OAUTH_SIGNIN_FAILURE';
                e.reason = body.error || '';
                return nextCallback(e);
            } else {
                res.locals.auth = JSON.parse(result.body);
                return nextCallback(null);
            }
        }
    ], err => next(err));
}


/**
 * Discord Access Token 으로 유저 정보를 획득
 */
function getDiscordUser (req, res, next) {
    async.waterfall([
        (nextCallback) => {
            request('https://discordapp.com/api/v6/users/@me', {
                headers: {
                    Authorization: `Bearer ${res.locals.auth.access_token}`
                }
            }, nextCallback);
        }, (result, body, nextCallback) => {
            if(result.statusCode !== 200) {
                let e = new Error('Forbidden');
                e.status = 403;
                e.code = 'ERROR_OAUTH_SIGNIN_FAILURE';
                e.reason = body.error || '';
                return nextCallback(e);
            } else {
                res.locals.me = JSON.parse(result.body);
                return nextCallback(null);
            }
        }
    ], err => next(err));
}

/**
 * 새로 받아온 정보를 등록 혹은 로그인 일자 업데이트
 * 대상이 되는 유저의 SEQ 값을 반환한다.
 */
function insertOrUpdateUserinfo (req, res, next) {
    async.waterfall([
        (nextCallback) => {
            let insertUserSQLString = 'INSERT INTO USERS_TBL(discord_id, displayname, email) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE last_login_datetime=NOW();'
            req.sqlConnection.query({
                sql: insertUserSQLString,
                values: [
                    res.locals.me.id,
                    res.locals.me.username,
                    res.locals.me.email
                ]
            }, nextCallback);
        }, (result, fields, nextCallback) => {
            res.locals.seq = result.insertId;
            return nextCallback(null);
        }
    ], err => next(err));
}

module.exports = {
    getDiscordInfo,
    getDiscordUser,
    insertOrUpdateUserinfo
}