'use strict';

/**
 * 토큰 리프레시를 위한 파라미터 세팅
 */
function assignToken (req, res, next) {
    res.locals.token = req.body.refresh_token;
    return next(null);
}

/**
 * token_data 의 정보의 유효성을 검사
 */
function checkRefreshToken (req, res, next) {
    if(!res.locals.token_data.seq) {
        let e = new Error('Forbidden');
        e.status = 403;
        e.code = 'ERROR_INVALID_TOKEN';
        return next(e);
    } else {
        res.locals.seq = res.locals.token_data.seq;
        return next(null);
    }
}

module.exports = {
    assignToken,
    checkRefreshToken
}