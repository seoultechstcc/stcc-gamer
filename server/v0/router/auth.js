'use strict';

const express = require('express');

const db = require('@dobedo-online-studio/prelude-mysql');

const authUtil = require('../util/passport');

const _discord = require('./auth.discord.ops');
const _token = require('./auth.token.ops');

let router = express.Router();

router.post('/signin/discord',
    _discord.getDiscordInfo,
    _discord.getDiscordUser,
    db.getConnection,
    _discord.insertOrUpdateUserinfo,
    db.endConnection,
    authUtil.makeAccessToken,
    authUtil.makeRefreshToken,
    (req, res) => {
        return res.send({
            access_expires: res.locals.access_expires,
            access_token: res.locals.access_token,
            refresh_expires:res.locals.refresh_expires,
            refresh_token:res.locals.refresh_token
        });
    }
);

router.post('/refresh',
    _token.assignToken,
    authUtil.decodeJwt,
    _token.checkRefreshToken,
    authUtil.makeAccessToken,
    authUtil.makeRefreshToken,
    (req, res) => {
        return res.send({
            access_expires: res.locals.access_expires,
            access_token: res.locals.access_token,
            refresh_expires:res.locals.refresh_expires,
            refresh_token:res.locals.refresh_token
        });
    }
);

module.exports = {
    router
};