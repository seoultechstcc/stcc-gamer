const configEnv = {
    // System Config
    mode: process.env.NODE_ENV || 'development',
    client_url: process.env.CLIENT_URL,
    api_url: process.env.API_URL,
    jwt_token_secret: process.env.JWT_TOKEN_SECRET,

    // MySQL
    mysql: {
        host: process.env.MYSQL_HOST,
        port: process.env.MYSQL_PORT,
        userID: process.env.MYSQL_USER_ID,
        userPassword : process.env.MYSQL_USER_PASSWORD,
        dbName : process.env.MYSQL_DB_NAME,
        connectionLimit : process.env.MYSQL_CONNECTION_LIMIT,
        keepAliveTime: process.env.MYSQL_KEEP_ALIVE_TIME || 60
    },

    // Discord
    discord: {
        client: process.env.DISCORD_CLIENT,
        secret: process.env.DISCORD_SECRET
    }
};



module.exports = configEnv;