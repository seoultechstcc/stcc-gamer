import { resource } from 'angular';

export interface IApiActionDescriptor extends resource.IActionDescriptor {
    noBearerToken: boolean;
}