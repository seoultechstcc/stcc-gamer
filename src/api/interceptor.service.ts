import { IRequestConfig, IHttpInterceptor } from 'angular';
import { IInjectable } from '@uirouter/core';

interface IInterceptorConfig extends IRequestConfig {
    noBearerToken: boolean;
}

export const ApiInterceptorInjectable: IInjectable = [
    'localStorageService',
    (localStorageService: any) => {
        return {
            request: function (config: IInterceptorConfig) {
                /* Bearer Token 세팅 로직 */
                if(!config.noBearerToken) {
                    let access_token = localStorageService.get('access_token');
                    if(access_token) {
                        config.headers['Authorization'] = `Bearer ${access_token}`;
                    }
                }
                return config;
            }
        }
    }
]