import { resource } from 'angular';
import { IApiRequestConfig } from '../constant/config.constant';
import { IApiActionDescriptor } from '../api.interface';

interface IDiscordLoginResult {
    access_token: string;
    refresh_token: string;
    access_expires_in: number;
    refresh_expires_in: number;
}

interface DiscordActionClass extends resource.IResourceClass<any> {
    signin: (params?: any) => resource.IResource<IDiscordLoginResult>
}

interface TokenActionClass extends resource.IResourceClass<any> {
    refresh: (params?: any) => resource.IResource<IDiscordLoginResult>
}

export class AuthRequestService {
    static $inject: string[] = ['$resource', 'requestConfig'];

    /* Members */
    public discord: DiscordActionClass
    public token: TokenActionClass
    public session: resource.IResourceClass<any>

    constructor (
        private $resource: resource.IResourceService,
        private requestConfig: IApiRequestConfig
    ) {
        this.discord = <DiscordActionClass>this.$resource(`${requestConfig.API_SERVER_URL}api/auth/signin/discord`, {}, {
            signin: <IApiActionDescriptor>{
                method: 'POST',
                noBearerToken: true
            }
        });

        this.token = <TokenActionClass>this.$resource(`${requestConfig.API_SERVER_URL}api/auth/:logic`, {}, {
            refresh: {
                method: 'POST',
                params: {
                    logic: 'refresh'
                }
            }
        });

        this.session = this.$resource(`${requestConfig.API_SERVER_URL}api/session`);
    }
}