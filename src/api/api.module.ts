import angular from 'angular';
import { RequestConfig } from './constant/config.constant';
import { AuthRequestService } from './request/auth.request';
import { ApiInterceptorInjectable } from './interceptor.service';

angular.module('sgApi', [])
.constant('requestConfig', RequestConfig)
.factory('interceptor', ApiInterceptorInjectable)
.service('authRequest', AuthRequestService)
;