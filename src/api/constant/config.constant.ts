export interface IApiRequestConfig {
    API_SERVER_URL: string;
}

export const RequestConfig: IApiRequestConfig = {
    API_SERVER_URL: 'http://localhost:3000/'
}