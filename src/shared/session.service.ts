import {} from 'angular';

export class SharedSessionService {

    static $inject: string[] = ['localStorageService']

    /* Members */
    constructor(
        protected localStorageService: any
    ) {
        console.log('SharedSessionService Init');
    }

    setAccessToken (access_token: string, refresh_token: string, expiresIn: number) {
        console.log(`SET access_token`);
        this.localStorageService.set('access_token', access_token);
        this.localStorageService.set('refresh_token', refresh_token);
        this.localStorageService.set('expiresIn', expiresIn);
    };

    removeAccessToken () {
        console.log(`REMOVE access_token`);
        this.localStorageService.remove('access_token');
        this.localStorageService.remove('refresh_token');
        this.localStorageService.remove('expiresIn');
    };
}