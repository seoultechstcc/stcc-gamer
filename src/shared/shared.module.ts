import ng from 'angular';
import { SharedSessionService } from './session.service';

ng.module('sgShared', [])
.service('sharedSessionService', SharedSessionService)
;