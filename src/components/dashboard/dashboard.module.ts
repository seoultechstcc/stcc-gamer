import angular from 'angular';
import { ViewDashboardComponent } from './view-dashboard/view-dashboard.component';

angular.module('sgDashboard', [])
.component('viewDashboard', ViewDashboardComponent)
;
