import './view-dashboard.scss';

import { IComponentOptions, IScope } from '../../../../node_modules/@types/angular';

export const ViewDashboardComponent: IComponentOptions = {
    template: require('./view-dashboard.template.html'),
    controller: class ViewDashboardCtrl {
        static $inject: string[] = ['$scope'];

        /* Members */
        constructor(
            private $scope: IScope,
        ) {
            console.log('ViewDashboardCtrl Init');
        }
    }
}