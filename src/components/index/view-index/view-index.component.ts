import './view-index.scss';

import { IComponentOptions, IScope } from '../../../../node_modules/@types/angular';

export const ViewIndexComponent: IComponentOptions = {
    template: require('./view-index.template.html'),
    controller: class ViewIndexCtrl {
        static $inject: string[] = ['$scope'];

        /* Members */
        constructor(
            private $scope: IScope,
        ) {
            console.log('ViewIndexCtrl Init');
        }
    }
}