import angular from 'angular';
import { ViewIndexComponent } from './view-index/view-index.component';

angular.module('sgIndex', [])
.component('viewIndex', ViewIndexComponent)
;
