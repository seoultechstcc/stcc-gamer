import './view-error.scss';

import { IComponentOptions, IComponentController, IScope } from 'angular';
import { StateService } from '@uirouter/core';

export const ViewErrorComponent: IComponentOptions = {
    template: require('./view-error.template.html'),
    controller: class ViewErrorCtrl implements IComponentController {
        static $inject: string[] = ['$scope', '$state'];

        /* Members */
        private errCode: string = '';
        private returnToUrl: string = '';

        constructor(
            private $scope: IScope,
            private $state: StateService
        ) {
            console.log('ViewErrorCtrl Init');
        }

        $onInit() {
            this.errCode = this.$state.params.code || 'ERROR_UNKNOWN';
            this.returnToUrl = this.$state.params.returnToUrl || '/';
        }
    }
}