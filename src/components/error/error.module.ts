import angular from 'angular';
import { ViewErrorComponent } from './view-error/view-error.component';

angular.module('sgError', [])
.component('viewError', ViewErrorComponent)
;