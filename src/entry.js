/**
 * vendors
 */
import 'jquery';
import 'angular';
import '@uirouter/angularjs';                      // ui.router
import 'angular-ui-bootstrap';                     // ui.bootstrap
import 'angular-resource';                         // ngResource
import 'angular-sanitize';                         // ngSanitize
import 'angular-local-storage';                    // LocalStorageModule,
import 'angular-http-auth';                        // http-auth-interceptor
import 'ng-file-upload';                           // ngFileUpload

/* Style */
import './stylesheets/theme.scss';

/* Core Module Import */
import './app.module';
import './api/api.module';
import './shared/shared.module';

/* Components */
import './components/index/index.module';
import './components/dashboard/dashboard.module';
import './components/error/error.module';