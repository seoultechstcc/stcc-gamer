import angular from 'angular';
import { Config } from './app.config';
import { Run } from './app.bootstrap';

angular.module('stccGamer', [
    'ui.router',
    'ui.bootstrap',
    'ngResource',
    'ngSanitize',
    'ngFileUpload',
    'LocalStorageModule',
    'http-auth-interceptor',

    /* sgModules */
    // Core Services
    'sgApi',
    'sgShared',

    // Components
    'sgIndex',
    'sgDashboard',
    'sgError',

])
.config(Config)
.run(Run)
;