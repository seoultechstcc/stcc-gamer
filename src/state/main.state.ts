import { Ng1StateDeclaration, IInjectable, StateService } from '../../node_modules/@uirouter/angularjs';
import { AuthRequestService } from '../api/request/auth.request';

const getSessionInjectable: IInjectable = [
    '$state',
    'authRequest',
    ($state: StateService, authRequest: AuthRequestService) => {
        return authRequest.session.get().$promise.then((session: any) => {
            return session;
        }).catch((err: any) => {
            console.log(err);
            $state.go('stccGamer.error', {
                code: (err.data) ? err.data.code : 'ERROR_UNKNOWN'
            });
        });
    }
]

export const MainState: Ng1StateDeclaration = {
    url: '/main',
    views: {
        'mainview': {
            component: 'viewDashboard'
        }
    },
    resolve: {
        loggedSession: getSessionInjectable
    }
}