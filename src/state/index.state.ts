import { Ng1StateDeclaration, IInjectable, StateService } from '../../node_modules/@uirouter/angularjs';

const checkTokenInjectable: IInjectable = [
    '$state',
    'localStorageService',
    ($state: StateService, localStorageService: any) => {
        let access_token = localStorageService.get('access_token');
        if(access_token) {
            $state.go('stccGamer.main');
        }
    }
]

export const IndexState: Ng1StateDeclaration = {
    url: '/',
    views: {
        'mainview': {
            component: 'viewIndex'
        }
    },
    resolve: {
        checkToken: checkTokenInjectable
    }
}