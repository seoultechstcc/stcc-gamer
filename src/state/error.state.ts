import { Ng1StateDeclaration } from '../../node_modules/@uirouter/angularjs';

export const ErrorState: Ng1StateDeclaration = {
    url: '/error?errCode?returnToUrl',
    views: {
        'mainview': {
            component: 'viewError'
        }
    }
}