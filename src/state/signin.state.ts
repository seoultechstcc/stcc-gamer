import { Ng1StateDeclaration, IInjectable, Transition, StateService, LocationServices } from '../../node_modules/@uirouter/angularjs';
import { AuthRequestService } from '../api/request/auth.request';
import { ILocationService } from 'angular';
import { SharedSessionService } from '../shared/session.service';

const signinDiscordInjectable: IInjectable = [
    '$transition$',
    '$state',
    '$location',
    'authRequest',
    'sharedSessionService',
    ($transition$: Transition, $state: StateService, $location:ILocationService, authRequest: AuthRequestService, sharedSessionService: SharedSessionService) => {
        let code = $transition$.params().code;

        return authRequest.discord.signin({
            code: code,
            redirect_uri: `${$location.absUrl().replace(/\?.*$/, '')}`
        }).$promise.then(res => {
            sharedSessionService.setAccessToken(res.access_token, res.refresh_token, res.access_expires_in);
            $state.go('stccGamer.main');
        }).catch(err => {
            console.log(err);
            $state.go('stccGamer.error', {
                code: (err.data) ? err.data.code : 'ERROR_UNKNOWN'
            });
        });
    }
]

export const SigninDefaultState: Ng1StateDeclaration = {
    url: '/signin',
    views: {
        'mainview': {
            template: '<span>Sign in ...</span>',
        }
    }
}

export const SigninDiscordState: Ng1StateDeclaration = {
    url: '/discord?code',
    resolve: {
        authInfo: signinDiscordInjectable
    }
}