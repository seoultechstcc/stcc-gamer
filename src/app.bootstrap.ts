import { IRootScopeService, httpAuth } from 'angular';
import { Trace, StateService } from '@uirouter/core';
import { SharedSessionService } from './shared/session.service';
import { AuthRequestService } from './api/request/auth.request';

export function Run(
    $rootScope: IRootScopeService, 
    $trace: Trace, 
    $state: StateService, 
    localStorageService: any, 
    authService: httpAuth.IAuthService,
    sharedSessionService: SharedSessionService,
    authRequest: AuthRequestService
) {
    console.log('App Running ... ');
    $trace.enable('TRANSITION');

    /* Session Refresh Logic */
    $rootScope.$on('event:auth-loginRequired', ($event, response) => {
        console.log(`Token Auth Error : ${response.data.reason}`);
        if(response.data.code === 'ERROR_TOKEN_EXPIRED') {
            console.log(`Start refresh logic`);
            authRequest.token.refresh({
                refresh_token: localStorageService.get('refresh_token')
            }).$promise.then(response => {
                sharedSessionService.setAccessToken(response.access_token, response.refresh_token, response.access_expires_in);
                authService.loginConfirmed();
            }).catch(err => {
                authService.loginCancelled();
            });
        } else {
            authService.loginCancelled();
        }
    });

    $rootScope.$on('event:auth-loginCancelled', ($event, response) => {
        console.log(`Login Canceled`);
        // sharedSessionService.disableUser();
        sharedSessionService.removeAccessToken();
        $state.go('stccGamer.index', {}, {
            reload: true
        });
    });


}