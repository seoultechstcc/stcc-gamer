import angular, { ILocationProvider } from 'angular';
import { StateProvider, UrlRouterProvider } from '@uirouter/angularjs';
import { IndexState } from './state/index.state';
import { MainState } from './state/main.state';
import { ErrorState } from './state/error.state';
import { SigninDiscordState, SigninDefaultState } from './state/signin.state';

export function Config (
    $stateProvider: StateProvider,
    $urlRouterProvider: UrlRouterProvider,
    $locationProvider: ILocationProvider,
    $httpProvider: any,
    localStorageServiceProvider: any,
) {
    localStorageServiceProvider.setPrefix('stccGamer');

    /* httpService Setting */
    $httpProvider.interceptors.push('interceptor');

    /* UI-Router Settings */
    $locationProvider.hashPrefix('!');
    $locationProvider.html5Mode(true);

    $stateProvider.state('stccGamer', {
        abstract: true,
        views: {
            '': {
                template: '<ui-view name="mainview"></ui-view>'
            }
        }
    })
    .state('stccGamer.index', IndexState)
    .state('stccGamer.error', ErrorState)
    .state('stccGamer.signin', SigninDefaultState)
    .state('stccGamer.signin.discord', SigninDiscordState)
    
    /* 여기서부터 애플리케이션 메인 */
    .state('stccGamer.main', MainState)
    ;
}