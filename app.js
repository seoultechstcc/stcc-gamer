'use strict';

/* Express Modules */
const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

/* Local Modules */
const config = require('./server.config');

/* Thirdparty API Calls */
const db = require('@dobedo-online-studio/prelude-mysql');

/* Thirdparty API Settings */
db.init({
    host: config.mysql.host,
    port: config.mysql.port,
    user: config.mysql.userID,
    password: config.mysql.userPassword,
    database: config.mysql.dbName,
    connectionLimit: config.mysql.connectionLimit,
    timezone: 'utc',
    keepAliveTime: config.mysql.keepAliveTime
});
db.getPool();

let app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
})); 
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/* API Version Define */
let version = {
    v0 : express.Router()
};

// Version V1
version.v0.use('/auth', require('./server/v0/router/auth').router);
version.v0.use('/session', require('./server/v0/router/session').router);

app.use('/api/v0', version.v0);

/* Default Api Version */
app.use('/api', version.v0);
app.use('/api/?', function(req, res, next) {
    var err = new Error('API Not Found');
    err.status = 404;
    next(err);
});

/* Default Static Endpoint */
app.all('/*', function (req, res, next) {
    if (req.accepts('html')) {
        res.sendFile('dist/index.html', {
            root: __dirname + "/public"
        });
    }
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});


/* Error 발생시 최초 처리 */
app.use((err, req, res, next) => {
    if(req.sqlConnection) {
        try {
            req.sqlConnection.release();
        } catch (error) {
            console.log(`Connection ${req.sqlConnection.threadId} Already released`);
        }
    }

    err.status = err.status || 500;
    if (err.status === 500) {
        console.error(`500 Error ${new Date().toUTCString()}`);
        console.error(err);
    }

    return next(err);
});

/* API Side Error Handler */
app.use('/api/?', function (err, req, res, next) {
    // set locals, only providing error in development
    let res_data = {};
    res_data.message = (err.status) ? err.message : 'Internal Server Error';
    res_data.reason = err.reason || '';
    res_data.code = err.code || 'ERROR_UNKNOWN';
    res_data.error = req.app.get('env') === 'development' ? err : {};
    res_data.stack = req.app.get('env') === 'development' ? err.stack : {};
    
    // Send error by JSON Reponse
    res.status(err.status || 500);
    res.send(res_data);
});

/* Render Side Error Handler */
app.use(function (err, req, res, next) {

    // set locals, only providing error in development
    res.locals.message = err.message || 'Internal Server Error';
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    
    /* render the error page */
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
