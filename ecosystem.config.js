try {
    var deployOptions = require('./pm2.deploy');
} catch (error) {
    var deployOptions = {};
    console.log('deploy option not included');
}

module.exports = {
    apps: [
        {
            name: 'stcc-gamer',
            script: 'bin/www',
            timezone: 'UTC',
            watch: ['server', 'app.js'],
            append_env_to_name: true,
            log_date_format: 'YYMMDD HH:mm:ss ',
            env: {
                NODE_ENV: 'development',
                PORT: 3000,
            },
            env_local_dev: deployOptions.development,
            env_local_production: deployOptions.production,
            env_remote_dev: {
                watch: false,
                PORT: 53380,
            },
            env_remote_production: {
                watch: false,
                NODE_ENV: 'production',
                PORT: 3000,
            }
        }
    ],
    deploy: deployOptions.deploy || {}
};